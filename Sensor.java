public class Sensor {

    /**
     * Available sensor states
     */
    public enum states {
        RELEASED,
        PRESSED
    }

    /**
     * State of the sensor
     */
    private states state;

    /**
     * Constructor, set state to RELEASED when initialed.
     */
    public Sensor()
    {
        this.state = states.RELEASED;
    }

    /**
     * constructor
     * @param state Set state when initialized
     */
    public Sensor(Sensor.states state)
    {
        this.state = state;
    }

    /**
     * Get sensor state
     * @return Sensor.states
     */
    public Sensor.states getState()
    {
        return state;
    }

    /**
     * Set sensor state
     * @param state
     */
    public synchronized void setState(Sensor.states state)
    {
        this.state = state;
    }
}