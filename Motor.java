// BEGIN MOTOR EXCEPTIONS
class SetMotorSpeedWhileOffException extends Exception
{
    @Override
    public String toString() {
        return "Cannot set motor speed while the motor is off";
    }
}

class SetMotorSpeedOutOfRangeException extends Exception
{
    @Override
    public String toString() {
        return "Cannot set motor speed to invalid value, check speedMax and speedMin for the valid motor speed range";
    }
}

class setMotorSpeedWhenInEmergencyStopException extends Exception
{
    @Override
    public String toString() {
        return "Cannot set motor speed because the motor is performing an emergency stop";
    }
}

class StopMotorWhileDrivingException extends Exception
{
    @Override
    public String toString() {
        return "Cannot stop motor while driving";
    }
}

class StopMotorWhileOffException extends Exception
{
    @Override
    public String toString() {
        return "Cannot stop motor while the motor is off";
    }
}

class StartMotorWhileDriving extends Exception
{
    @Override
    public String toString() {
        return "Cannot start motor while the motor is driving";
    }
}

class StartMotorWhileIdle extends Exception
{
    @Override
    public String toString() {
        return "Cannot stop motor while the motor is idle";
    }
}
// END MOTOR EXCEPTIONS


public class Motor {

    /**
     * Motor states
     */
    public enum MotorStates {
        OFF,
        IDLE,
        DRIVING_FORWARD,
        DRIVING_REVERSE,
    }

    /**
     * Motor state
     */
    private MotorStates state;

    /**
     * Motor speed
     */
    private int speed;

    /**
     * This is set to true when the SpeedControler perfoms a
     */
    private boolean manuelInputBlock = false;

    /**
     * Miximum Motor speed
     */
    public static int speedMax = 100;

    /**
     * Minimum Motor speed
     */
    public static int speedMin = -100;

    /**
     * Motor speed when the Motor state equals IDLE
     */
    public static int speedIdle = 0;


    /**
     * Motor state getter
     * @return Motor.MotorDirections
     */
    public MotorStates getState() {
        return this.state;
    }

    /**
     * Return if the motor is driving
     * @return boolean
     */
    public boolean IsDriving()
    {
        return this.state == MotorStates.DRIVING_FORWARD || this.state == MotorStates.DRIVING_REVERSE;
    }

    /**
     * IsManuelInputBlock
     * @return boolean
     */
    public boolean IsManuelInputBlocked()
    {
        return this.manuelInputBlock;
    }

    /**
     * Block manual speed input
     */
    public void blockManuelInput()
    {
        this.manuelInputBlock = true;
    }

    /**
     * Unblock manual speed input
     */
    public void unblockManuelInput()
    {
        this.manuelInputBlock = false;
    }

    /**
     * Motor speed getter
     * @return int
     */
    public int getSpeed() {
        return this.speed;
    }

    /**
     * Constructor
     */
    public Motor()
    {
        this.state = MotorStates.OFF;
        this.speed = speedIdle;
    }

    /**
     * Start the Motor
     * @throws StartMotorWhileIdle
     * @throws StartMotorWhileDriving
     */
    public synchronized void start() throws StartMotorWhileIdle, StartMotorWhileDriving
    {
        // Throw exception when Motor state equals IDLE
        if (this.state == MotorStates.IDLE) {
            throw new StartMotorWhileIdle();
        }

        // Throw exception when Motor state equals DRIVING
        if (this.IsDriving()) {
            throw new StartMotorWhileDriving();
        }

        this.state = MotorStates.IDLE;
        this.speed = 0;
    }

    /**
     * Shutdown the Motor
     * @throws StopMotorWhileOffException
     * @throws StopMotorWhileDrivingException
     */
    public synchronized void shutdown() throws StopMotorWhileOffException, StopMotorWhileDrivingException
    {
        // Throw exception when Motor state equals OFF
        if (this.state == MotorStates.OFF) {
            throw new StopMotorWhileOffException();
        }

        // Throw exception when Motor state equals DRIVING
        if (this.IsDriving()) {
            throw new StopMotorWhileDrivingException();
        }

        this.state = MotorStates.OFF;
    }

    /**
     * Set Motor speed
     * @param speed
     * @throws SetMotorSpeedWhileOffException
     * @throws SetMotorSpeedOutOfRangeException
     */
    public synchronized void setSpeedManual(int speed) throws
            SetMotorSpeedWhileOffException,
            SetMotorSpeedOutOfRangeException,
            setMotorSpeedWhenInEmergencyStopException
    {
        if (!this.manuelInputBlock) {
            this.setSpeed(speed);
        } else {
            throw new setMotorSpeedWhenInEmergencyStopException();
        }
    }

    /**
     * Set Motor speed
     * @param speed
     * @throws SetMotorSpeedWhileOffException
     * @throws SetMotorSpeedOutOfRangeException
     */
    public synchronized void setSpeed(int speed) throws
            SetMotorSpeedWhileOffException, SetMotorSpeedOutOfRangeException
    {
        // Throw exception when the Motor state equals OFF
        if (this.state == MotorStates.OFF) {
            throw new SetMotorSpeedWhileOffException();
        }

        // When a negative number is given while the direction is FORWARD then the motor will speed wil be set to 0.
        // The same will happen when the direction is REVERSE and a positive number is given.
        if ((speed < 0 && this.state == MotorStates.DRIVING_FORWARD) || (speed > 0 && this.state == MotorStates.DRIVING_REVERSE)) {
            this.speed = 0;
        }

        // Throw exception when the speed is out of range.
        if (speed < speedMin || speed > speedMax) {
            throw new SetMotorSpeedOutOfRangeException();
        }

        // Update motor speed
        this.speed = speed;

        // If the speed is equal to speedIdle then set the Motor state to Idle, anything else set the Motor state to DRIVING.
        if (this.speed > speedIdle){
            this.state = MotorStates.DRIVING_FORWARD;
        } else if (this.speed < speedIdle) {
            this.state = MotorStates.DRIVING_REVERSE;
        } else {
            this.state = MotorStates.IDLE;
        }
    }
}