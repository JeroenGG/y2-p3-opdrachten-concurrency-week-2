import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class GUI extends JFrame implements ActionListener, Runnable {
    /**
     * GUI Text field for the motor speed
     */
    private JTextField tfSpeed = new JTextField("50");

    /**
     * GUI Button that will be used to simulate starting the motor
     */
    private JButton btnStart = new JButton("Start");

    /**
     * GUI Button that will be used to simulate stopping the Motor
     */
    private JButton btnStop = new JButton("Stop");

    /**
     * GUI Button that will be used to update the Motor speed
     */
    private JButton btnDrive = new JButton("Drive");

    /**
     * GUI Checkbox that will be used to select the Left Motor
     */
    private JCheckBox cbMotorLeft = new JCheckBox("Motor Left");

    /**
     * GUI Checkbox that will be used to select the Right Motor
     */
    private JCheckBox cbMotorRight = new JCheckBox("Motor Right");

    /**
     * GUI Checkbox that will be used to simulate the Left Sensor
     */
    private JCheckBox cbSensorLeft = new JCheckBox("Sensor Left");

    /**
     * GUI Checkbox that will be used to simulate the Right Sensor
     */
    private JCheckBox cbSensorRight = new JCheckBox("Sensor Right");

    /**
     * GUI Label
     */
    private JLabel lblLeftMotorTitle = new JLabel("Motor left");

    /**
     * GUI Label that will show the state of the Motor
     */
    private JLabel lblLeftMotorState = new JLabel("Motor State: ");

    /**
     * GUI Label that will show the speed of the Motor
     */
    private JLabel lblLeftMotorSpeed = new JLabel("Motor Speed: ");

    /**
     * GUI Label that will show the state of the Sensor
     */
    private JLabel lblLeftSensorState = new JLabel("Sensor State: ");

    /**
     * GUI Label
     */
    private JLabel lblRightMotorTitle = new JLabel("Motor right");

    /**
     * GUI Label that will show the state of the Motor
     */
    private JLabel lblRightMotorState = new JLabel("Motor State: ");

    /**
     * GUI Label that will show the speed of the Motor
     */
    private JLabel lblRightMotorSpeed = new JLabel("Motor Speed: ");

    /**
     * GUI Label that will show the state of the Sensor
     */
    private JLabel lblRightSensorState = new JLabel("Sensor State: ");

    /**
     * Left Motor
     */
    private Motor motorLeft;

    /**
     * Right Motor
     */
    private Motor motorRight;

    /**
     * Left Sensor
     */
    private Sensor sensorLeft;

    /**
     * Right Sensor
     */
    private Sensor sensorRight;

    /**
     * Setup GUI
     */
    public GUI(Motor motorLeft, Motor motorRight, Sensor sensorLeft, Sensor sensorRight)
    {
        this.setTitle("Transducer");

        // Set motors & sensors
        this.motorLeft = motorLeft;
        this.motorRight = motorRight;
        this.sensorLeft = sensorLeft;
        this.sensorRight = sensorRight;

        // BEGIN GUI LAYOUT
        this.getContentPane().setLayout(null);

        // Add textfield to GUI
        this.tfSpeed.setEnabled(false);
        this.tfSpeed.setBounds(new Rectangle(10, 10, 160, 30));
        this.getContentPane().add(this.tfSpeed);

        // Add buttons to GUI
        this.btnStop.setEnabled(false);
        this.btnDrive.setEnabled(false);
        this.btnStart.setBounds(new Rectangle(new Point(10, 50), this.btnStart.getPreferredSize()));
        this.btnStop.setBounds(new Rectangle(new Point(this.btnStart.getBounds().width + this.btnStart.getBounds().x + 10, 50), this.btnStop.getPreferredSize()));
        this.btnDrive.setBounds(new Rectangle(new Point(this.btnStop.getBounds().width + this.btnStop.getBounds().x + 10, 50), this.btnDrive.getPreferredSize()));
        this.getContentPane().add(this.btnStart);
        this.getContentPane().add(this.btnStop);
        this.getContentPane().add(this.btnDrive);

        // Add checkboxes to GUI
        this.cbMotorLeft.setSelected(true);
        this.cbMotorLeft.setBounds(new Rectangle(10, 80, 100, 20));
        this.cbMotorRight.setBounds(new Rectangle(120, 80, 100, 20));
        this.cbSensorLeft.setBounds(new Rectangle(10, 100, 100, 20));
        this.cbSensorRight.setBounds(new Rectangle(120, 100, 100, 20));
        this.getContentPane().add(this.cbMotorLeft);
        this.getContentPane().add(this.cbMotorRight);
        this.getContentPane().add(this.cbSensorLeft);
        this.getContentPane().add(this.cbSensorRight);

        // Add label to GUI
        this.lblLeftMotorTitle.setBounds(new Rectangle(new Point(10, 140), this.lblLeftMotorTitle.getPreferredSize()));
        this.lblLeftMotorState.setBounds(new Rectangle(new Point(10, 160), this.lblLeftMotorState.getPreferredSize()));
        this.lblLeftMotorSpeed.setBounds(new Rectangle(new Point(10, 180), this.lblLeftMotorSpeed.getPreferredSize()));
        this.lblLeftSensorState.setBounds(new Rectangle(new Point(10, 200), this.lblLeftSensorState.getPreferredSize()));
        this.getContentPane().add(this.lblLeftMotorTitle);
        this.getContentPane().add(this.lblLeftMotorState);
        this.getContentPane().add(this.lblLeftMotorSpeed);
        this.getContentPane().add(this.lblLeftSensorState);

        this.lblRightMotorTitle.setBounds(new Rectangle(220, 140, 100, 20));
        this.lblRightMotorState.setBounds(new Rectangle(new Point(220, 160), this.lblRightMotorState.getPreferredSize()));
        this.lblRightMotorSpeed.setBounds(new Rectangle(new Point(220, 180), this.lblRightMotorSpeed.getPreferredSize()));
        this.lblRightSensorState.setBounds(new Rectangle(new Point(220, 200), this.lblRightSensorState.getPreferredSize()));
        this.getContentPane().add(this.lblRightMotorTitle);
        this.getContentPane().add(this.lblRightMotorState);
        this.getContentPane().add(this.lblRightMotorSpeed);
        this.getContentPane().add(this.lblRightSensorState);


        // Create button & checkbox listeners
        this.btnStart.addActionListener(this);
        this.btnStop.addActionListener(this);
        this.btnDrive.addActionListener(this);
        this.cbMotorLeft.addActionListener(this);
        this.cbMotorRight.addActionListener(this);
        this.cbSensorLeft.addActionListener(this);
        this.cbSensorRight.addActionListener(this);

        // Disable checkbox focus
        this.cbMotorLeft.setFocusable(false);
        this.cbMotorRight.setFocusable(false);
        this.cbSensorLeft.setFocusable(false);
        this.cbSensorRight.setFocusable(false);

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(450, 260);
        // END GUI LAYOUT

        // Update GUI
        this.updateGUI();

        // Show window
        this.setVisible(true);

        // Start update thread.
        this.run();
    }

    /**
     * GUI update function
     */
    public void run() {
        while (true) {

            this.updateGUI();

            try {
                Thread.sleep(10);
            } catch (Exception e) {}
        }
    }

    /**
     * This method is called when a Button or Checkbox is pressed.
     * @param e ActionEvent
     */
    public void actionPerformed(ActionEvent e) {
        // GUI Button "start" is pressed
        if (e.getSource() == this.btnStart) {
            Motor motor = (this.cbMotorLeft.isSelected()) ? this.motorLeft : this.motorRight;
            try {
                motor.start();
            } catch (Exception ex) {
                this.showMessageDialog(ex.getMessage());
            }
        }

        // GUI Button "stop" is pressed
        if (e.getSource() == this.btnStop) {
            Motor motor = (this.cbMotorLeft.isSelected()) ? this.motorLeft : this.motorRight;
            try {
                motor.shutdown();
            } catch (Exception ex) {
                this.showMessageDialog(ex.getMessage());
            }
        }

        // GUI Button "accelerate" is pressed
        if (e.getSource() == this.btnDrive) {
            Motor motor = (this.cbMotorLeft.isSelected()) ? this.motorLeft : this.motorRight;

            try {
                Integer speed = Integer.parseInt(this.tfSpeed.getText());
                motor.setSpeedManual(speed);
            } catch (Exception ex) {
                this.showMessageDialog(ex.getMessage());
            }
        }

        if (e.getSource() == this.cbMotorLeft) {
            this.cbMotorRight.setSelected(!this.cbMotorLeft.isSelected());
            this.tfSpeed.setText(Integer.toString(this.cbMotorLeft.isSelected() ? this.motorLeft.getSpeed() : this.motorRight.getSpeed()));
        }

        if (e.getSource() == this.cbMotorRight) {
            this.cbMotorLeft.setSelected(!this.cbMotorRight.isSelected());
            this.tfSpeed.setText(Integer.toString(this.cbMotorLeft.isSelected() ? this.motorLeft.getSpeed() : this.motorRight.getSpeed()));
        }

        if (e.getSource() == this.cbSensorLeft) {
            this.sensorLeft.setState(this.cbSensorLeft.isSelected() ? Sensor.states.PRESSED : Sensor.states.RELEASED);
        }

        if (e.getSource() == this.cbSensorRight) {
            this.sensorRight.setState(this.cbSensorRight.isSelected() ? Sensor.states.PRESSED : Sensor.states.RELEASED);
        }
    }


    /**
     * Update GUI labels
     */
    private void updateGUI() {
        // Update labels
        updateLabel(this.lblLeftMotorState, "Motor State: " + this.motorLeft.getState().toString());
        updateLabel(this.lblLeftMotorSpeed, "Motor Speed: " + this.motorLeft.getSpeed());
        updateLabel(this.lblLeftSensorState, "Sensor State: " + this.sensorLeft.getState().toString());

        updateLabel(this.lblRightMotorState, "Motor State: " + this.motorRight.getState().toString());
        updateLabel(this.lblRightMotorSpeed, "Motor Speed: " + this.motorRight.getSpeed());
        updateLabel(this.lblRightSensorState, "Sensor State: " + this.sensorRight.getState().toString());

        if (this.cbMotorLeft.isSelected()) {
            this.btnStart.setEnabled(this.motorLeft.getState() == Motor.MotorStates.OFF && !this.motorLeft.IsManuelInputBlocked());
            this.btnStop.setEnabled(this.motorLeft.getState() == Motor.MotorStates.IDLE && !this.motorLeft.IsManuelInputBlocked());
            this.btnDrive.setEnabled(this.motorLeft.getState() != Motor.MotorStates.OFF && !this.motorLeft.IsManuelInputBlocked());
            this.tfSpeed.setEnabled(this.motorLeft.getState() != Motor.MotorStates.OFF && !this.motorLeft.IsManuelInputBlocked());
        } else {
            this.btnStart.setEnabled(this.motorRight.getState() == Motor.MotorStates.OFF && !this.motorRight.IsManuelInputBlocked());
            this.btnStop.setEnabled(this.motorRight.getState() == Motor.MotorStates.IDLE && !this.motorRight.IsManuelInputBlocked());
            this.btnDrive.setEnabled(this.motorRight.getState() != Motor.MotorStates.OFF && !this.motorRight.IsManuelInputBlocked());
            this.tfSpeed.setEnabled(this.motorRight.getState() != Motor.MotorStates.OFF && !this.motorRight.IsManuelInputBlocked());
        }
    }

    /**
     * Update label text
     * @param btn
     * @param text
     */
    private void updateLabel(JLabel btn, String text) {
        btn.setText(text);
        btn.setBounds(new Rectangle(new Point(btn.getX(), btn.getY()), btn.getPreferredSize()));
    }

    /**
     * Show Message
     * @param msg
     */
    private void showMessageDialog(String msg) {
        JOptionPane.showMessageDialog(this, msg);
    }
}