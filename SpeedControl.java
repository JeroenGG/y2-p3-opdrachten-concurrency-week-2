public class SpeedControl implements Runnable {

    /**
     * Motor
     */
    private Motor motor;

    /**
     * Sensor
     */
    private Sensor sensor;

    /**
     * Last sensor state, this is used to check if the sensor state has changed
     */
    private Sensor.states sensorLastState;

    /**
     * Backup speed when the motor is in an emergency stop
     */
    private static Integer MOTOR_BACKUP_SPEED = 10;

    /**
     * Constructor
     * @param motor
     * @param sensor
     */
    public SpeedControl(Motor motor, Sensor sensor) {
        this.motor = motor;
        this.sensor = sensor;
    }

    @Override
    public void run() {
        while(true) {
            Sensor.states sensorState = this.sensor.getState();
            if (sensorState != this.sensorLastState && sensorState == Sensor.states.PRESSED && this.motor.IsDriving()) {
                try {
                    this.motor.blockManuelInput();
                    this.stopMotor();
                    this.motor.unblockManuelInput();
                } catch (Exception e) {}
            }
            sensorLastState = sensorState;

            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {}
        }
    }

    /**
     * Emergency stop the motor
     * @throws SetMotorSpeedWhileOffException
     * @throws SetMotorSpeedOutOfRangeException
     * @throws InterruptedException
     */
    private void stopMotor() throws
        SetMotorSpeedWhileOffException, SetMotorSpeedOutOfRangeException,
        InterruptedException
    {
        Motor.MotorStates state = this.motor.getState();

        // Set motor to IDLE
        try {
            this.motor.setSpeed(Motor.speedIdle);
        } catch(SetMotorSpeedWhileOffException e) {
            throw new SetMotorSpeedWhileOffException();
        } catch(SetMotorSpeedOutOfRangeException e) {
            throw new SetMotorSpeedOutOfRangeException();
        }

        // BACKUP
        Thread.sleep(2000);
        try {
            this.motor.setSpeed(state == Motor.MotorStates.DRIVING_FORWARD ? -MOTOR_BACKUP_SPEED : MOTOR_BACKUP_SPEED);
        } catch(SetMotorSpeedWhileOffException e) {
            throw new SetMotorSpeedWhileOffException();
        } catch(SetMotorSpeedOutOfRangeException e) {
            throw new SetMotorSpeedOutOfRangeException();
        }

        while (this.sensor.getState() == Sensor.states.PRESSED) {
            try {
                Thread.sleep(10);
            } catch (Exception e) {}
        }

        try {
            this.motor.setSpeed(Motor.speedIdle);
        } catch(SetMotorSpeedWhileOffException e) {
            throw new SetMotorSpeedWhileOffException();
        } catch(SetMotorSpeedOutOfRangeException e) {
            throw new SetMotorSpeedOutOfRangeException();
        }
    }
}