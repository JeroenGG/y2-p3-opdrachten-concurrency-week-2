public class main {
    public static void main(String[] args) {
        Motor motorLeft = new Motor();
        Motor motorRight = new Motor();
        Sensor sensorLeft = new Sensor();
        Sensor sensorRight = new Sensor();

        Thread speedControlLeft = new Thread(new SpeedControl(motorLeft, sensorLeft));
        Thread speedControlRight = new Thread(new SpeedControl(motorRight, sensorRight));
        speedControlLeft.start();
        speedControlRight.start();

        GUI gui = new GUI(motorLeft, motorRight, sensorLeft, sensorRight);
    }
}